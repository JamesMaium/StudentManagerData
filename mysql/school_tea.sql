/*
 Navicat Premium Data Transfer

 Source Server         : 校园茶舍管理系统
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : school_tea

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 13/02/2022 21:05:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for datil_table
-- ----------------------------
DROP TABLE IF EXISTS `datil_table`;
CREATE TABLE `datil_table`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shopId` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `count` int(10) NOT NULL,
  `dateTime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user`(`userId`) USING BTREE,
  INDEX `shop`(`shopId`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 520 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of datil_table
-- ----------------------------
INSERT INTO `datil_table` VALUES (470, 53, 47, 3, '2022-01-27 01:26:05');
INSERT INTO `datil_table` VALUES (469, 95, 47, 2, '2022-01-27 01:26:05');
INSERT INTO `datil_table` VALUES (468, 57, 47, 4, '2022-01-27 01:26:05');
INSERT INTO `datil_table` VALUES (467, 2, 47, 3, '2022-01-27 01:25:39');
INSERT INTO `datil_table` VALUES (466, 96, 1, 1, '2022-01-24 05:15:16');
INSERT INTO `datil_table` VALUES (465, 95, 1, 1, '2022-01-24 05:15:16');
INSERT INTO `datil_table` VALUES (464, 61, 1, 1, '2022-01-24 05:15:16');
INSERT INTO `datil_table` VALUES (463, 120, 50, 1, '2022-01-24 05:12:37');
INSERT INTO `datil_table` VALUES (462, 64, 50, 1, '2022-01-24 05:12:37');
INSERT INTO `datil_table` VALUES (461, 61, 50, 1, '2022-01-24 05:12:37');
INSERT INTO `datil_table` VALUES (460, 5, 50, 1, '2022-01-24 05:12:37');
INSERT INTO `datil_table` VALUES (459, 120, 49, 1, '2022-01-24 05:05:48');
INSERT INTO `datil_table` VALUES (458, 50, 1, 1, '2022-01-24 05:03:32');
INSERT INTO `datil_table` VALUES (457, 92, 1, 1, '2022-01-24 05:03:32');
INSERT INTO `datil_table` VALUES (456, 118, 1, 1, '2022-01-24 05:03:32');
INSERT INTO `datil_table` VALUES (455, 6, 1, 1, '2022-01-24 05:03:32');
INSERT INTO `datil_table` VALUES (454, 2, 1, 1, '2022-01-24 05:03:32');
INSERT INTO `datil_table` VALUES (453, 50, 49, 1, '2022-01-24 05:02:41');
INSERT INTO `datil_table` VALUES (452, 6, 49, 1, '2022-01-24 05:02:41');
INSERT INTO `datil_table` VALUES (451, 46, 49, 1, '2022-01-24 05:02:41');
INSERT INTO `datil_table` VALUES (450, 34, 49, 1, '2022-01-24 05:02:41');
INSERT INTO `datil_table` VALUES (449, 85, 1, 1, '2022-01-24 04:55:01');
INSERT INTO `datil_table` VALUES (448, 84, 1, 1, '2022-01-24 04:55:01');
INSERT INTO `datil_table` VALUES (447, 9, 1, 1, '2022-01-24 04:55:01');
INSERT INTO `datil_table` VALUES (446, 118, 1, 1, '2022-01-24 04:55:01');
INSERT INTO `datil_table` VALUES (445, 2, 1, 1, '2022-01-24 04:55:01');
INSERT INTO `datil_table` VALUES (444, 64, 47, 2, '2022-01-24 04:52:20');
INSERT INTO `datil_table` VALUES (443, 51, 47, 1, '2022-01-24 04:52:20');
INSERT INTO `datil_table` VALUES (442, 50, 47, 1, '2022-01-24 04:52:20');
INSERT INTO `datil_table` VALUES (441, 7, 47, 1, '2022-01-24 04:52:20');
INSERT INTO `datil_table` VALUES (440, 6, 47, 1, '2022-01-24 04:52:20');
INSERT INTO `datil_table` VALUES (471, 66, 51, 1, '2022-02-11 14:03:26');
INSERT INTO `datil_table` VALUES (472, 67, 51, 1, '2022-02-11 14:03:26');
INSERT INTO `datil_table` VALUES (473, 64, 51, 1, '2022-02-11 14:03:26');
INSERT INTO `datil_table` VALUES (474, 65, 51, 1, '2022-02-11 14:03:26');
INSERT INTO `datil_table` VALUES (475, 99, 51, 1, '2022-02-11 14:05:14');
INSERT INTO `datil_table` VALUES (476, 50, 51, 1, '2022-02-11 14:05:14');
INSERT INTO `datil_table` VALUES (477, 41, 51, 1, '2022-02-11 14:05:14');
INSERT INTO `datil_table` VALUES (478, 120, 51, 1, '2022-02-11 14:05:14');
INSERT INTO `datil_table` VALUES (479, 4, 51, 1, '2022-02-11 14:44:12');
INSERT INTO `datil_table` VALUES (480, 5, 51, 1, '2022-02-11 14:44:12');
INSERT INTO `datil_table` VALUES (481, 9, 51, 1, '2022-02-11 14:44:12');
INSERT INTO `datil_table` VALUES (482, 120, 51, 2, '2022-02-11 14:44:12');
INSERT INTO `datil_table` VALUES (483, 106, 51, 5, '2022-02-11 19:31:30');
INSERT INTO `datil_table` VALUES (484, 107, 51, 1, '2022-02-11 19:31:30');
INSERT INTO `datil_table` VALUES (485, 108, 51, 1, '2022-02-11 19:31:30');
INSERT INTO `datil_table` VALUES (486, 122, 51, 2, '2022-02-11 19:33:24');
INSERT INTO `datil_table` VALUES (487, 122, 1, 1, '2022-02-11 19:34:20');
INSERT INTO `datil_table` VALUES (488, 122, 1, 1, '2022-02-11 19:34:25');
INSERT INTO `datil_table` VALUES (489, 122, 1, 1, '2022-02-11 19:34:28');
INSERT INTO `datil_table` VALUES (490, 124, 51, 2, '2022-02-11 21:29:37');
INSERT INTO `datil_table` VALUES (491, 125, 51, 2, '2022-02-11 21:29:37');
INSERT INTO `datil_table` VALUES (492, 126, 51, 1, '2022-02-11 21:29:37');
INSERT INTO `datil_table` VALUES (493, 130, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (494, 131, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (495, 132, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (496, 133, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (497, 123, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (498, 134, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (499, 124, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (500, 135, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (501, 125, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (502, 126, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (503, 127, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (504, 128, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (505, 129, 51, 1, '2022-02-12 14:13:00');
INSERT INTO `datil_table` VALUES (506, 132, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (507, 133, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (508, 134, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (509, 124, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (510, 135, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (511, 125, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (512, 126, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (513, 127, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (514, 128, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (515, 129, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (516, 130, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (517, 131, 51, 1, '2022-02-13 19:04:03');
INSERT INTO `datil_table` VALUES (518, 62, 51, 1, '2022-02-13 21:03:22');
INSERT INTO `datil_table` VALUES (519, 63, 51, 1, '2022-02-13 21:03:22');

-- ----------------------------
-- Table structure for shop_table
-- ----------------------------
DROP TABLE IF EXISTS `shop_table`;
CREATE TABLE `shop_table`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` decimal(10, 2) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 137 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shop_table
-- ----------------------------
INSERT INTO `shop_table` VALUES (2, '香蕉一斤(Banana)', 20.00, NULL, '2022-01-14 06:26:00', '水果');
INSERT INTO `shop_table` VALUES (120, '元气森林', 5.00, '', '2022-01-24 05:05:06', '甜点饮品');
INSERT INTO `shop_table` VALUES (55, '哈密瓜', 5.00, '', '2022-01-20 03:42:09', '水果');
INSERT INTO `shop_table` VALUES (4, '打火机', 3.00, NULL, '2022-01-14 06:26:00', '超市便利店');
INSERT INTO `shop_table` VALUES (5, '方便面', 5.20, NULL, '2022-01-14 06:26:00', '超市便利店');
INSERT INTO `shop_table` VALUES (6, '冰红茶', 2.99, NULL, '2022-01-14 06:26:00', '下午茶');
INSERT INTO `shop_table` VALUES (7, '茉莉蜜茶', 2.99, NULL, '2022-01-14 06:26:00', '下午茶');
INSERT INTO `shop_table` VALUES (32, '大白菜', 1.90, '', '2022-01-20 03:34:42', '买菜');
INSERT INTO `shop_table` VALUES (9, '黑兰州香烟(一盒)', 20.00, NULL, '2022-01-14 06:26:00', '超市便利店');
INSERT INTO `shop_table` VALUES (10, '黑兰州香烟(一条)', 200.00, NULL, '2022-01-14 06:26:00', '超市便利店');
INSERT INTO `shop_table` VALUES (33, '胡萝卜', 2.99, '', '2022-01-20 03:34:54', '买菜');
INSERT INTO `shop_table` VALUES (12, '老干妈', 8.00, NULL, '2022-01-14 06:26:00', '超市便利店');
INSERT INTO `shop_table` VALUES (13, '香菜', 3.00, NULL, '2022-01-14 06:26:00', '超市便利店');
INSERT INTO `shop_table` VALUES (40, '沐浴露', 30.00, '', '2022-01-20 03:36:42', '超市便利店');
INSERT INTO `shop_table` VALUES (34, '黄瓜', 1.99, '', '2022-01-20 03:35:00', '买菜');
INSERT INTO `shop_table` VALUES (35, '洋芋（土豆、马铃薯）', 3.00, '', '2022-01-20 03:35:21', '买菜');
INSERT INTO `shop_table` VALUES (36, '农夫山泉', 2.00, '', '2022-01-20 03:35:32', '超市便利店');
INSERT INTO `shop_table` VALUES (37, '农夫山泉大瓶2.5L', 10.00, '', '2022-01-20 03:35:57', '超市便利店');
INSERT INTO `shop_table` VALUES (38, '农夫山泉一提（24瓶）', 40.00, '', '2022-01-20 03:36:21', '超市便利店');
INSERT INTO `shop_table` VALUES (116, '鸡爪（小）', 1.00, '', '2022-01-24 04:38:22', '超市便利店');
INSERT INTO `shop_table` VALUES (41, '白砂糖', 4.00, '', '2022-01-20 03:37:50', '甜点饮品');
INSERT INTO `shop_table` VALUES (43, '蜜雪冰城 奶昔', 20.00, '', '2022-01-20 03:38:19', '甜点饮品');
INSERT INTO `shop_table` VALUES (44, '蜜雪冰城 冰镇柠檬水', 5.00, '', '2022-01-20 03:38:41', '甜点饮品');
INSERT INTO `shop_table` VALUES (45, '老冰棍', 1.00, '', '2022-01-20 03:39:17', '甜点饮品');
INSERT INTO `shop_table` VALUES (46, '美味汉堡', 20.00, '', '2022-01-20 03:39:51', '汉堡');
INSERT INTO `shop_table` VALUES (47, '华莱士汉堡', 20.00, '', '2022-01-20 03:40:04', '汉堡');
INSERT INTO `shop_table` VALUES (59, '烤羊腿', 100.00, '', '2022-01-22 06:13:32', '晚饭');
INSERT INTO `shop_table` VALUES (58, '麦当劳', 15.99, '', '2022-01-22 06:13:20', '汉堡');
INSERT INTO `shop_table` VALUES (50, '手抓饼', 6.00, '', '2022-01-20 03:40:37', '美食');
INSERT INTO `shop_table` VALUES (51, '混沌', 3.00, '', '2022-01-20 03:40:47', '美食');
INSERT INTO `shop_table` VALUES (52, '葡萄', 5.00, '', '2022-01-20 03:41:04', '水果');
INSERT INTO `shop_table` VALUES (53, '草莓', 10.00, '', '2022-01-20 03:41:10', '水果');
INSERT INTO `shop_table` VALUES (56, '西瓜（保熟的西瓜）', 10.00, '', '2022-01-20 03:42:39', '水果');
INSERT INTO `shop_table` VALUES (57, '大棚的西瓜（保熟）', 20.00, '', '2022-01-20 03:43:09', '水果');
INSERT INTO `shop_table` VALUES (60, '啤酒', 50.00, '', '2022-01-22 06:13:43', '超市便利店');
INSERT INTO `shop_table` VALUES (61, '炒面', 10.00, '', '2022-01-22 06:13:59', '中午饭');
INSERT INTO `shop_table` VALUES (62, '牛肉面', 10.00, '', '2022-01-22 06:14:07', '中午饭');
INSERT INTO `shop_table` VALUES (63, '小菜', 1.00, '', '2022-01-22 06:14:15', '中午饭');
INSERT INTO `shop_table` VALUES (64, '小烧烤', 10.00, '', '2022-01-22 06:16:05', '夜宵');
INSERT INTO `shop_table` VALUES (65, '烤鱿鱼', 10.00, '', '2022-01-22 06:16:18', '夜宵');
INSERT INTO `shop_table` VALUES (66, '烤面筋', 10.00, '', '2022-01-22 06:16:24', '夜宵');
INSERT INTO `shop_table` VALUES (67, '油炸年糕', 2.00, '', '2022-01-22 06:16:40', '夜宵');
INSERT INTO `shop_table` VALUES (68, '油炸面筋', 2.00, '', '2022-01-22 06:16:55', '夜宵');
INSERT INTO `shop_table` VALUES (69, '自助火锅（一位）', 39.00, '', '2022-01-22 06:17:17', '晚饭');
INSERT INTO `shop_table` VALUES (70, '三鲜火锅', 100.00, '', '2022-01-22 06:17:32', '晚饭');
INSERT INTO `shop_table` VALUES (71, '火锅底料', 10.00, '', '2022-01-22 06:17:41', '晚饭');
INSERT INTO `shop_table` VALUES (72, '炒面条', 10.00, '', '2022-01-22 06:17:49', '晚饭');
INSERT INTO `shop_table` VALUES (73, '炒小菜', 10.00, '', '2022-01-22 06:17:56', '晚饭');
INSERT INTO `shop_table` VALUES (74, '小汉堡', 15.00, '', '2022-01-22 06:18:10', '汉堡');
INSERT INTO `shop_table` VALUES (75, '大白菜', 2.00, '', '2022-01-22 06:18:20', '买菜');
INSERT INTO `shop_table` VALUES (76, '菠菜', 1.00, '', '2022-01-22 06:18:27', '买菜');
INSERT INTO `shop_table` VALUES (77, '韭菜', 2.00, '', '2022-01-22 06:18:34', '买菜');
INSERT INTO `shop_table` VALUES (78, '香菜', 1.50, '', '2022-01-22 06:18:47', '买菜');
INSERT INTO `shop_table` VALUES (79, '胡萝卜', 2.00, '', '2022-01-22 06:18:54', '买菜');
INSERT INTO `shop_table` VALUES (80, '黄瓜', 2.00, '', '2022-01-22 06:19:00', '买菜');
INSERT INTO `shop_table` VALUES (81, '西红柿', 2.00, '', '2022-01-22 06:19:05', '买菜');
INSERT INTO `shop_table` VALUES (82, '小雪糕', 3.00, '', '2022-01-22 06:19:17', '甜点饮品');
INSERT INTO `shop_table` VALUES (83, '蒙牛雪糕', 5.00, '', '2022-01-22 06:19:27', '甜点饮品');
INSERT INTO `shop_table` VALUES (84, '农夫山泉', 2.00, '', '2022-01-22 06:19:34', '甜点饮品');
INSERT INTO `shop_table` VALUES (85, '恒大冰泉', 5.00, '', '2022-01-22 06:19:42', '甜点饮品');
INSERT INTO `shop_table` VALUES (86, '冰红茶', 4.00, '', '2022-01-22 06:20:02', '甜点饮品');
INSERT INTO `shop_table` VALUES (87, '绿茶', 3.00, '', '2022-01-22 06:20:11', '甜点饮品');
INSERT INTO `shop_table` VALUES (88, '脉动', 5.00, '', '2022-01-22 06:20:16', '甜点饮品');
INSERT INTO `shop_table` VALUES (89, '红牛', 6.00, '', '2022-01-22 06:20:21', '甜点饮品');
INSERT INTO `shop_table` VALUES (90, '体质能量', 6.00, '', '2022-01-22 06:20:28', '甜点饮品');
INSERT INTO `shop_table` VALUES (91, '铁观音', 30.00, '', '2022-01-22 06:20:36', '下午茶');
INSERT INTO `shop_table` VALUES (92, '猫屎咖啡', 20.00, '', '2022-01-22 06:20:42', '下午茶');
INSERT INTO `shop_table` VALUES (93, '星巴克（4杯）', 90.00, '', '2022-01-22 06:20:52', '下午茶');
INSERT INTO `shop_table` VALUES (94, '星巴克1杯', 23.00, '', '2022-01-22 06:21:06', '下午茶');
INSERT INTO `shop_table` VALUES (95, '荔枝', 20.00, '', '2022-01-22 06:21:21', '水果');
INSERT INTO `shop_table` VALUES (96, '雪梨', 5.00, '', '2022-01-22 06:21:30', '水果');
INSERT INTO `shop_table` VALUES (97, '葡萄', 20.00, '', '2022-01-22 06:21:39', '水果');
INSERT INTO `shop_table` VALUES (99, '天水凉皮', 5.00, '', '2022-01-22 06:28:57', '美食');
INSERT INTO `shop_table` VALUES (100, '天水呱呱', 6.00, '', '2022-01-22 06:29:03', '美食');
INSERT INTO `shop_table` VALUES (101, '天水手抓凉粉', 6.00, '', '2022-01-22 06:29:13', '美食');
INSERT INTO `shop_table` VALUES (102, '手抓饼', 7.00, '', '2022-01-22 06:29:24', '美食');
INSERT INTO `shop_table` VALUES (103, '小笼包', 20.00, '', '2022-01-22 06:29:32', '美食');
INSERT INTO `shop_table` VALUES (104, '大笼包', 30.00, '', '2022-01-22 06:29:40', '美食');
INSERT INTO `shop_table` VALUES (105, '胡辣汤（河南）', 4.00, '', '2022-01-22 06:30:00', '美食');
INSERT INTO `shop_table` VALUES (106, '胡辣汤（陕西）', 5.00, '', '2022-01-22 06:30:14', '美食');
INSERT INTO `shop_table` VALUES (107, '小米粥', 2.00, '', '2022-01-22 06:30:21', '美食');
INSERT INTO `shop_table` VALUES (108, '醪糟', 2.00, '', '2022-01-22 06:30:27', '美食');
INSERT INTO `shop_table` VALUES (122, '梦之蓝（500ML）', 800.00, '', '2022-02-11 19:33:00', '超市便利店');
INSERT INTO `shop_table` VALUES (124, '蒸熊掌', 180.00, '', '2022-02-11 21:25:01', '美食');
INSERT INTO `shop_table` VALUES (125, '蒸鹿尾儿', 130.00, '', '2022-02-11 21:25:28', '美食');
INSERT INTO `shop_table` VALUES (126, '烧花鸭', 80.00, '', '2022-02-11 21:25:34', '美食');
INSERT INTO `shop_table` VALUES (127, '烧雏鸡', 40.00, '', '2022-02-11 21:25:41', '美食');
INSERT INTO `shop_table` VALUES (128, '烧子鹅', 30.00, '', '2022-02-11 21:25:50', '美食');
INSERT INTO `shop_table` VALUES (129, '卤猪', 80.00, '', '2022-02-11 21:25:58', '美食');
INSERT INTO `shop_table` VALUES (130, '卤鸭', 40.00, '', '2022-02-11 21:26:06', '美食');
INSERT INTO `shop_table` VALUES (131, '酱鸡', 45.00, '', '2022-02-11 21:26:13', '美食');
INSERT INTO `shop_table` VALUES (132, '腊肉', 50.00, '', '2022-02-11 21:26:21', '美食');
INSERT INTO `shop_table` VALUES (133, '松花', 20.00, '', '2022-02-11 21:26:29', '美食');
INSERT INTO `shop_table` VALUES (134, '小肚儿', 20.00, '', '2022-02-11 21:26:37', '美食');
INSERT INTO `shop_table` VALUES (135, '晾肉', 30.00, '', '2022-02-11 21:26:45', '美食');

-- ----------------------------
-- Table structure for user_table
-- ----------------------------
DROP TABLE IF EXISTS `user_table`;
CREATE TABLE `user_table`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userPassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `regitTime` datetime(0) NULL DEFAULT NULL,
  `userTrueName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `jurisdiction` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `balance` decimal(20, 2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`userName`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 53 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_table
-- ----------------------------
INSERT INTO `user_table` VALUES (33, 'wangfugui', 'wangfugui', '2022-01-22 06:32:31', '王富贵', '普通会员', 540.04);
INSERT INTO `user_table` VALUES (31, 'admin', 'admin', '2022-01-22 06:12:09', '马中华', '管理员', 37681.14);
INSERT INTO `user_table` VALUES (30, 'zhaoliu', 'zhaoliu', '2022-01-17 02:56:33', '赵六', '普通会员', 100000.00);
INSERT INTO `user_table` VALUES (29, 'wangwu', 'wangwu', '2022-01-17 02:56:17', '王五', '普通会员', 100000.00);
INSERT INTO `user_table` VALUES (32, 'zhangsanfeng', 'zhangsanfeng', '2022-01-22 06:31:27', '张三丰', '普通会员', 0.00);
INSERT INTO `user_table` VALUES (47, 'zhangsan', '123456', '2022-01-24 04:51:17', '张三', '普通会员', 9855.02);
INSERT INTO `user_table` VALUES (28, 'lisi', 'lisi', '2022-01-17 02:56:02', '李四', '普通会员', 99899.10);
INSERT INTO `user_table` VALUES (34, 'liuergou', 'liuergou', '2022-01-22 06:33:05', '刘二狗', '普通会员', 0.00);
INSERT INTO `user_table` VALUES (35, 'wanger', 'wanger', '2022-01-22 06:33:28', '王二', '普通会员', 0.00);
INSERT INTO `user_table` VALUES (36, 'zhaoyifa', 'zhaoyifa', '2022-01-22 06:33:56', '赵一发', '普通会员', 0.00);
INSERT INTO `user_table` VALUES (1, '1', '123123123', '2022-01-22 06:59:34', '游客', '游客', 99997084.16);
INSERT INTO `user_table` VALUES (50, 'mazhonghua', 'mazhonghua', '2022-01-24 05:11:19', '马中华', '普通会员', 69.80);
INSERT INTO `user_table` VALUES (51, 'jamesma', 'jamesma', '2022-02-11 14:02:54', '马中华', '普通会员', 6170.80);
INSERT INTO `user_table` VALUES (52, 'test1', 'test1', '2022-02-13 20:20:48', '测试1', '普通会员', 0.00);
