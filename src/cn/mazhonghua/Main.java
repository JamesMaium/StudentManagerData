package cn.mazhonghua;

import cn.mazhonghua.view.LoginView;

/**
 * @author JamesMaium
 * Start
 *  校园茶舍点餐项目管理系统
 *  兰州工业学院
 *  21专升本软件工程2班 马中华
 * （1）游客功能：查询商品信息、点餐、下单、付款。
 * （2）会员功能：查询会员信息、修改密码、余额充值、查询商品信息、点餐、下单、付款。
 * （3）管理员：商品信息的管理、会员信息的管理、订单的管理、统计。
 */
public class Main {
    public static void main(String[] args) {
        new LoginView();
    }
}
