package cn.mazhonghua.dao;

/**
 * @author JamesMaium
 */
public class RegitUserDataDao {
    String userName;
    String userPassword;
    String userAgainPassword;
    String name;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserAgainPassword() {
        return userAgainPassword;
    }

    public void setUserAgainPassword(String userAgainPassword) {
        this.userAgainPassword = userAgainPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
