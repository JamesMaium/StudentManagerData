package cn.mazhonghua.config;


/**
 * @author JamesMaium
 * 配置中心
 */
public class JFrameConfig {
    public static final String LOGIN_TITLE = "校园茶舍点餐系统-登录";
    public static final String TITLE = "校园茶舍点餐系统-后台管理";
    public static final String REGIT_TITLE = "校园茶舍点餐系统-注册";
    public static final String REGIT_FRAME_TITLE = "欢迎注册";
    public static final String USER_UI_TITLE = "欢迎登录:";
    public static final String USER_UI_TOP_IMG = "src\\cn\\mazhonghua\\images\\topBack.png";
    public static final String TEST_IMG = "src\\cn\\mazhonghua\\images\\testimg.png";
    public static final String LOCALHOST = "jdbc:mysql://localhost:3306/school_tea?useSSL=false";
    public static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
    public static final String USER_NAME = "school_tea";
    public static final String PASSWORD = "school_tea";
    public static final int WIDTH = 1465;
    public static final int HEIGHT = 980;
    public static final String LOGIN_WINDOW_ICON = "src\\cn\\mazhonghua\\images\\set.png";
    public static final String TOPBACKIMG = "src\\cn\\mazhonghua\\images\\img.png";

    public static final int ADMIN_WIDTH = 400;
    public static final int ADMIN_HEIGHT = 600;
    public static final String ADMIN_INDEX_TITLE = "欢迎登录,尊贵的管理员:";
    public static final int ADMIN_USER_MANAGER_WIDTH = 800;
    public static final int ADMIN_USER_MANAGER_HEIGHT = 500;
}
