package cn.mazhonghua.service;

import cn.mazhonghua.util.MysqlConnUtil;
import cn.mazhonghua.view.AdminDeleteShopItem;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 * 删除商品 逻辑
 */
public class AdminDeleteShopItemEvents {
    public AdminDeleteShopItemEvents(AdminDeleteShopItem flag) {
        try {
            List<String> arr = new ArrayList<>();
            List<String> id = new ArrayList<>();
            List<HashMap<String, String>> shopItem = new MysqlConnUtil().getShopItem();
            int a = 0;
            for (HashMap<String, String> map : shopItem) {
                arr.add(a, map.get("id") + ":" + map.get("title") + "[" + map.get("price") + "]");
                id.add(a, map.get("id"));
                a++;
            }
            flag.itemArr = arr;
            flag.IdArr = id;


            flag.comboBox.setFont(new Font("黑体", Font.PLAIN, 16));
            flag.comboBox.removeAllItems();
            for (String s : arr) {
                flag.comboBox.addItem(s);
            }
            flag.title.setText("剩余总数：" + arr.size());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public AdminDeleteShopItemEvents(AdminDeleteShopItem flag, int i) {
        try {
            int selectedIndex = flag.comboBox.getSelectedIndex();
            int deleteId = new Integer(flag.IdArr.get(selectedIndex));
            String s = flag.itemArr.get(selectedIndex);
            System.out.println(s);
            System.out.println(deleteId);
            boolean b = new MysqlConnUtil().delShopItemById(deleteId);
            if (!b) {
                JOptionPane.showMessageDialog(flag, "删除失败", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
            } else {
                new AdminDeleteShopItemEvents(flag);
                JOptionPane.showMessageDialog(flag, "删除成功", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
