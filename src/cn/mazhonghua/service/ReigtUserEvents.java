package cn.mazhonghua.service;

import cn.mazhonghua.dao.RegitUserDataDao;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author JamesMaium
 * 用户注册逻辑处理
 */
public class ReigtUserEvents {
    public ReigtUserEvents(RegitUserDataDao userDataDao, JFrame regitJframe, JFrame loginJframe){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(new Date());
        String userName = userDataDao.getUserName();
        String userPassword = userDataDao.getUserPassword();
        String name = userDataDao.getName();
        String sql = "insert into user_table values (0,'"+userName+"','"+userPassword+"','"+time+"','"+name+"','普通会员',0)";
        try {
            new MysqlConnUtil().insetSql(sql);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            JOptionPane.showMessageDialog(regitJframe, "注册失败！"+e.getMessage(), "JamesMa提示",JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        JOptionPane.showMessageDialog(regitJframe, "注册成功！", "JamesMa提示",JOptionPane.INFORMATION_MESSAGE);
        loginJframe.setEnabled(true);
        regitJframe.dispose();
    }
}
