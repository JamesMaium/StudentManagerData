package cn.mazhonghua.service;

import cn.mazhonghua.util.MysqlConnUtil;
import cn.mazhonghua.view.MineLog;
import cn.mazhonghua.view.UserCenter;

import javax.swing.*;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * @author JamesMaium
 * 用户中心 逻辑
 */
public class UserCenterEvents {
    public UserCenterEvents(){}
    public UserCenterEvents(String id, UserCenter userCenter, String numberMoney) {
        try {
            boolean math = isMath(numberMoney);
            if (!math) {
                JOptionPane.showMessageDialog(userCenter, "请输入一个正整数", "提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            boolean b = new MysqlConnUtil().rechargeMoney(new Integer(id), new Integer(numberMoney));
            if(b){
                JOptionPane.showMessageDialog(userCenter, "充值成功", "提示", JOptionPane.WARNING_MESSAGE);
                double oldBalance = Double.parseDouble(userCenter.balance.getText());
                int integer1 = new Integer(numberMoney);
                double newBalance = oldBalance+integer1;
                userCenter.balance.setText(String.valueOf(newBalance));
                userCenter.recharge.setText("");
            }else{
                JOptionPane.showMessageDialog(userCenter, "充值失败", "提示", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public UserCenterEvents(UserCenter userCenter, String password) {
        try {
            if(password.toCharArray().length<6){
                JOptionPane.showMessageDialog(userCenter, "新密码必须大于6位数哦？", "提示", JOptionPane.ERROR_MESSAGE);
                return;
            }
            Integer id = new Integer(userCenter.userAllInfo.get("id"));
            boolean b = new MysqlConnUtil().updateUserPassword(id, password);
            if(b){
                JOptionPane.showMessageDialog(userCenter, "修改成功", "提示", JOptionPane.WARNING_MESSAGE);
                userCenter.pawd.setText("");
            }else{
                JOptionPane.showMessageDialog(userCenter, "修改失败", "提示", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public UserCenterEvents(UserCenter userCenter) {
        try {
            new MineLog(userCenter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isMath(String number) {
        String reg = "^[0-9]*[1-9][0-9]*$";
        return Pattern.compile(reg).matcher(number).find();
    }
    public boolean isSmallMath(String number) {
        String reg = "^\\+{0,1}[1-9]\\d*";
        return Pattern.compile(reg).matcher(number).find();
    }

}
