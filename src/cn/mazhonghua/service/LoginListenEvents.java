package cn.mazhonghua.service;

import cn.mazhonghua.util.MysqlConnUtil;
import cn.mazhonghua.view.AdminIndex;
import cn.mazhonghua.view.RegitUserView;
import cn.mazhonghua.view.UserView;

import javax.swing.*;
import java.util.HashMap;

/**
 * @author JamesMaium
 * 用户登录逻辑
 */
public class LoginListenEvents {
    private final static String JURIS = "普通会员";
    private final static String MANAGER = "管理员";
    private final static String TOURIST = "游客登录";


    public LoginListenEvents(JFrame jFrame) {
        jFrame.setEnabled(false);
        new RegitUserView(jFrame);
    }

    public LoginListenEvents(JFrame jFrame,int i){
        jFrame.dispose();
        new UserView(TOURIST, i);
    }

    public LoginListenEvents(JFrame jFrame, String username, String password) {
        try {
            if (username.toCharArray().length == 0 || password.toCharArray().length == 0) {
                JOptionPane.showMessageDialog(jFrame, "账户名或密码不能是空", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            String userTrueName = new MysqlConnUtil().userLogin(username, password);
            if (userTrueName.toCharArray().length == 0) {
                JOptionPane.showMessageDialog(jFrame, "账户名或密码错误", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            HashMap<String, Object> map = new MysqlConnUtil().queryUserPrice(username);
            String juris = (String) map.get("jurisdiction");
            if (JURIS.equals(juris)) {
                jFrame.dispose();
                new UserView(userTrueName, (int) map.get("id"));
            } else if (MANAGER.equals(juris)) {
                jFrame.dispose();
                new AdminIndex(userTrueName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
