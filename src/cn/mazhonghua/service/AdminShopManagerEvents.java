package cn.mazhonghua.service;

import cn.mazhonghua.view.AdminShopManager;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;

public class AdminShopManagerEvents {
    public AdminShopManagerEvents(JTable table, AdminShopManager flag, int i) {
        try {
            String queryData = flag.queryData;
            int pageSize = flag.pageSize;
            int currPage = flag.currPage;
            if (i == 0) {
                if (currPage == 1) {
                    return;
                }
                flag.currPage-=1;
            }
            if(i==1){
                flag.currPage+=1;
            }
            String[] head = flag.head;
            String[][] tableData = flag.getTableData(queryData);
            DefaultTableModel dtm = new DefaultTableModel(tableData,head);
            table.setModel(dtm);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
