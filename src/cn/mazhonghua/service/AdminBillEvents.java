package cn.mazhonghua.service;

import cn.mazhonghua.util.MysqlConnUtil;
import cn.mazhonghua.view.AdminBill;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 */
public class AdminBillEvents {
    public AdminBillEvents() {

    }

    public AdminBillEvents(AdminBill flag, int i) throws SQLException {
        if (i == 0) {
            if (flag.currPage == 1) {
                return;
            }
            flag.currPage -= 1;
            String[][] data = getData(flag.currPage, flag.pageSize, flag.head.length);
            DefaultTableModel dtm = new DefaultTableModel(data,flag.head);
            flag.table.setModel(dtm);
        } else {
            flag.currPage += 1;
            String[][] data = getData(flag.currPage, flag.pageSize, flag.head.length);
            DefaultTableModel dtm = new DefaultTableModel(data,flag.head);
            flag.table.setModel(dtm);
        }

    }

    public AdminBillEvents(AdminBill flag) {
        try {
            String[][] data = getData(flag.currPage, flag.pageSize, flag.head.length);
            DefaultTableModel dtm = new DefaultTableModel(data, flag.head);
            flag.table.setModel(dtm);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[][] getData(int currPage, int pageSize, int headLength) throws SQLException {
        List<HashMap<String, String>> datilData = new MysqlConnUtil().getDatilData(currPage, pageSize);
        String[][] data = new String[datilData.size()][headLength];
        int a = 0;
        for (HashMap<String, String> map : datilData) {
            data[a][0] = map.get("datilId");
            data[a][1] = map.get("shopTitle");
            data[a][2] = map.get("shopPrice");
            data[a][3] = map.get("shopType");
            data[a][4] = map.get("userTrueName");
            data[a][5] = map.get("userName");
            data[a][6] = map.get("count");
            data[a][7] = map.get("datilTime");
            a++;
        }
        return data;
    }
}
