package cn.mazhonghua.service;

import cn.mazhonghua.dao.ShopItemDao;
import cn.mazhonghua.util.MysqlConnUtil;
import cn.mazhonghua.view.AdminAddShopItem;

import javax.swing.*;
import java.sql.SQLException;

/**
 * @author JamesMaium
 * 添加商品 逻辑
 */
public class AdminAddShopItemEvents {
    public AdminAddShopItemEvents(AdminAddShopItem flag, ShopItemDao shopItem){
        try {

            if(shopItem.getPrice().length()==0||shopItem.getName().length()==0){
                JOptionPane.showMessageDialog(flag, "添加失败！不能留空哦", "提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            boolean math = new UserCenterEvents().isSmallMath(shopItem.getPrice());
            if(!math){
                JOptionPane.showMessageDialog(flag, "添加失败！输入价格不合理", "提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            boolean b = new MysqlConnUtil().addShop(shopItem);
            if(!b){
                JOptionPane.showMessageDialog(flag, "添加失败！", "提示",JOptionPane.ERROR_MESSAGE);
                return;
            }
            JOptionPane.showMessageDialog(flag, "添加成功！", "提示",JOptionPane.INFORMATION_MESSAGE);
            flag.type.setSelectedItem(0);
            flag.priceField.setText("");
            flag.nameField.setText("");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
