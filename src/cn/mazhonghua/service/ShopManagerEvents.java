package cn.mazhonghua.service;

import cn.mazhonghua.view.AdminIndex;
import cn.mazhonghua.view.AdminShopManager;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;

/**
 * @author JamesMaium
 * 商品管理 逻辑
 */
public class ShopManagerEvents {
    public ShopManagerEvents(AdminIndex flag,String name){
        try {
            new AdminShopManager(flag,name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public ShopManagerEvents(AdminShopManager flag, String data, JTable table){
        try {
            flag.queryData = data;
            String[][] tableData = flag.getTableData(data);
            String[] head = flag.head;
            DefaultTableModel dtm = new DefaultTableModel(tableData,head);
            table.setModel(dtm);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
