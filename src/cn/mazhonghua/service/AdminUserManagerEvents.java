package cn.mazhonghua.service;

import cn.mazhonghua.view.AdminUserManager;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.SQLException;

/**
 * @author JamesMaium
 */
public class AdminUserManagerEvents {
    public AdminUserManagerEvents(JTable table, AdminUserManager flag, int i, JLabel numLable) {
        try {
            int pageSize = flag.pageSize;
            int currPage = flag.currPage;
            String[][] data = null;
            if (i == 0) {
                if(currPage==1){
                    return;
                }
                flag.currPage--;
                data = flag.getData(currPage -= 1, pageSize);
            }
            if (i == 1) {
                flag.currPage++;
                data = flag.getData(currPage += 1, pageSize);
            }
            numLable.setText("第" + currPage + "页,每页：" + pageSize + "条");
            String[] head = flag.head;
            DefaultTableModel dtm = new DefaultTableModel(data, head);
            table.setModel(dtm);
            table.getColumn("id").setMaxWidth(40);
            table.getColumn("id").setMinWidth(40);
            table.getColumn("注册时间").setMaxWidth(200);
            table.getColumn("注册时间").setMinWidth(200);
            table.setRowHeight(40);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
