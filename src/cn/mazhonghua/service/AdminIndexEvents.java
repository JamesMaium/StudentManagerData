package cn.mazhonghua.service;

import cn.mazhonghua.util.MysqlConnUtil;
import cn.mazhonghua.view.AdminIndex;
import cn.mazhonghua.view.AdminUserManager;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * @author JamesMaium
 * 后台管理逻辑
 */
public class AdminIndexEvents {
    public AdminIndexEvents(AdminIndex flag,String name){
        try {
            new AdminUserManager(flag,name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public AdminIndexEvents(AdminIndex flag){
        try {
            HashMap<String, String> map = new MysqlConnUtil().showAdminData();
            flag.userCount.setText(map.get("user_count"));
            flag.todayMoney.setText(map.get("today_price"));
            flag.adminCount.setText(map.get("admin_count"));
            flag.shopCount.setText(map.get("shop_count"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
