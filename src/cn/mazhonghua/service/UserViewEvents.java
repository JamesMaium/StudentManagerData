package cn.mazhonghua.service;

import cn.mazhonghua.util.MysqlConnUtil;
import cn.mazhonghua.view.Settlement;
import cn.mazhonghua.view.UserView;

import javax.swing.*;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * @author JamesMaium
 * 用户界面 逻辑
 */
public class UserViewEvents {
    public UserViewEvents(String id, JLabel countLable, JLabel priceLable, JFrame flag, HashMap<String,String> tempMapCars) {
        double price = Double.parseDouble(priceLable.getText());
        int integer = new Integer(countLable.getText());
        if(tempMapCars.containsKey(id)){
            String s = tempMapCars.get(id);
            tempMapCars.put(id,String.valueOf(new Integer(s)+1));
        }else{
            tempMapCars.put(id,String.valueOf(1));
        }
        try {
            double thisPrice = new MysqlConnUtil().quaryPriceById(new Integer(id));
            DecimalFormat decimalFormat = new DecimalFormat("#.00");
            String format = decimalFormat.format(price + thisPrice);
            priceLable.setText(format);
            countLable.setText(String.valueOf(integer + 1));
            flag.repaint();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public UserViewEvents(JLabel priceLable, UserView flag, int id, HashMap<String,String> map) {
        new Settlement(priceLable, flag,id,map);
    }
}
