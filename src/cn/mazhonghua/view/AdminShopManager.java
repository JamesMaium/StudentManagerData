package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.AdminShopManagerEvents;
import cn.mazhonghua.service.AdminUserManagerEvents;
import cn.mazhonghua.service.ShopManagerEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 * 商品管理 UI
 */
public class AdminShopManager extends JFrame {


    public int currPage = 1;
    public int pageSize = 10;
    private final static String TITLE = "商品管理页面";
    public String[] head = {"id", "标题", "价格", "上架时间", "类型"};
    public JTable table = new JTable();
    public JLabel numText = new JLabel("",JLabel.CENTER);
    public String queryData = "";

    public AdminShopManager(AdminIndex flag, String name) throws SQLException {
        flag.setEnabled(false);
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);
        JPanel jPanel = new JPanel();
        jPanel.setBackground(Color.white);
        jPanel.setLayout(flowLayout);
        jPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT));
        add(jPanel);

        JPanel topPanel = new JPanel();
        topPanel.setLayout(flowLayout);
        topPanel.setSize(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, 100);

        JTextField queryData = new JTextField();
        queryData.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100 - 150, 40));
        topPanel.add(queryData);

        JButton okBtn = new JButton("搜索");
        okBtn.setPreferredSize(new Dimension(150, 40));
        okBtn.setBorder(null);
        okBtn.setBackground(Color.pink);
        okBtn.addActionListener(e -> new ShopManagerEvents(this, queryData.getText(), table));
        topPanel.add(okBtn);
        jPanel.add(topPanel);

        JPanel bodyPanel = new JPanel();
        bodyPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT - 150));
        bodyPanel.setLayout(flowLayout);

        String[][] tableData = getTableData("");
        DefaultTableModel dtm = new DefaultTableModel(tableData, head);
        table.setModel(dtm);
        table.setRowHeight(40);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT - 150));
        bodyPanel.add(scrollPane);

        jPanel.add(bodyPanel);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setBackground(new Color(255,255,255));
        bottomPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, 50));
        bottomPanel.setLayout(flowLayout);
        JButton leftBtn = new JButton();
        leftBtn.setPreferredSize(new Dimension(100,30));
        leftBtn.setText("上一页");
        leftBtn.setBackground(new Color(51, 171, 68));
        leftBtn.setForeground(new Color(255,255,255));
        leftBtn.setBorder(null);
        leftBtn.addActionListener(e->new AdminShopManagerEvents(table,this,0));
        bottomPanel.add(leftBtn);

        numText.setText("第"+currPage+"页，每页"+pageSize+"条");
        numText.setPreferredSize(new Dimension(150,30));
        bottomPanel.add(numText);

        JButton rightBtn = new JButton();
        rightBtn.setPreferredSize(new Dimension(100,30));
        rightBtn.setText("下一页");
        rightBtn.setBackground(new Color(45, 111, 197));
        rightBtn.setForeground(new Color(255,255,255));
        rightBtn.setBorder(null);
        rightBtn.addActionListener(e->new AdminShopManagerEvents(table,this,1));
        bottomPanel.add(rightBtn);

        JButton addShopBtn = new JButton();
        addShopBtn.setPreferredSize(new Dimension(100,30));
        addShopBtn.setText("增加商品");
        addShopBtn.setBackground(new Color(35, 243, 71));
        addShopBtn.setForeground(new Color(255,255,255));
        addShopBtn.setBorder(null);
        addShopBtn.addActionListener(e->new AdminAddShopItem(this,name));
        bottomPanel.add(addShopBtn);

        JButton delShopBtn = new JButton();
        delShopBtn.setPreferredSize(new Dimension(100,30));
        delShopBtn.setText("删除商品");
        delShopBtn.setBackground(new Color(232, 27, 83));
        delShopBtn.setForeground(new Color(255,255,255));
        delShopBtn.setBorder(null);
        delShopBtn.addActionListener(e->new AdminDeleteShopItem(name));
        bottomPanel.add(delShopBtn);

        jPanel.add(bottomPanel);

        setLayout(flowLayout);
        setBackground(new Color(255, 255, 255));
        setTitle(TITLE + name);
        setSize(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                flag.setEnabled(true);
            }
        });
    }

    public String[][] getTableData(String data) throws SQLException {
        List<HashMap<String, String>> shopList = new MysqlConnUtil().getShopList(data, (currPage - 1) * pageSize, pageSize);
        String[][] dataArr = new String[shopList.size()][head.length];
        int a = 0;
        for (HashMap<String, String> maps : shopList) {
            dataArr[a][0] = maps.get("id");
            dataArr[a][1] = maps.get("title");
            dataArr[a][2] = maps.get("price");
            dataArr[a][3] = maps.get("datetime");
            dataArr[a][4] = maps.get("type");
            a++;
        }
        numText.setText("第"+currPage+"页，每页"+pageSize+"条");
        return dataArr;
    }
}
