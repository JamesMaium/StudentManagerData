package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.LoginListenEvents;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author JamesMaium
 * 登录页面
 */
public class LoginView extends JFrame {
    final private LoginView flag = this;
    public LoginView() {
        setIconImage(new ImageIcon(JFrameConfig.LOGIN_WINDOW_ICON).getImage());
        setTitle(JFrameConfig.LOGIN_TITLE);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BorderLayout borderLayout = new BorderLayout();
        setLayout(borderLayout);
        setSize(500, 400);
        JPanel topPanel = new JPanel();
        topPanel.setBackground(new Color(255, 255, 255));
        JLabel topLable = new JLabel();
        topLable.setPreferredSize(new Dimension(500, 150));
        topLable.setIcon(new ImageIcon(JFrameConfig.TOPBACKIMG));
        topLable.setFont(new Font("微软雅黑", Font.BOLD, 30));
        topPanel.add(topLable);
        add(topPanel, BorderLayout.NORTH);
        JPanel centerPanel = new JPanel();
        add(centerPanel, BorderLayout.CENTER);
        centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        centerPanel.setBackground(new Color(255, 255, 255));
        JPanel centerTop = new JPanel();
        centerTop.setBackground(new Color(255, 255, 255));
        centerTop.setLayout(new FlowLayout(FlowLayout.LEFT));
        centerTop.setPreferredSize(new Dimension(300, 40));
        JTextField userLoginFleid = new JTextField();
        Font font = new Font("黑体", Font.PLAIN, 18);
        userLoginFleid.setFont(font);
        userLoginFleid.setPreferredSize(new Dimension(225, 30));
        JLabel userLoginText = new JLabel("用户名:");
        userLoginText.setFont(font);
        centerTop.add(userLoginText);
        centerTop.add(userLoginFleid);
        JPanel centerBottom = new JPanel();
        centerBottom.setBackground(new Color(255, 255, 255));
        centerBottom.setLayout(new FlowLayout(FlowLayout.LEFT));
        centerBottom.setPreferredSize(new Dimension(300, 40));
        JLabel userPasswordText = new JLabel("密  码:");
        userPasswordText.setFont(font);
        centerBottom.add(userPasswordText);
        JPasswordField userPasswordFleid = new JPasswordField();
        userPasswordFleid.setPreferredSize(new Dimension(225, 30));
        centerBottom.add(userPasswordFleid);
        JPanel loginPanel = new JPanel();
        loginPanel.setPreferredSize(new Dimension(300, 50));
        JButton loginBtn = new JButton("登   录");
        loginBtn.setPreferredSize(new Dimension(300, 40));
        loginBtn.setBackground(new Color(70, 70, 215));
        loginBtn.setForeground(new Color(255, 255, 255));
        loginBtn.setFont(font);
        loginPanel.setBackground(new Color(255, 255, 255));
        loginPanel.add(loginBtn);
        loginBtn.addActionListener((e) -> {
            new LoginListenEvents(this, userLoginFleid.getText(), new String(userPasswordFleid.getPassword()));
        });
        JPanel regitPanel = new JPanel();
        regitPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        regitPanel.setPreferredSize(new Dimension(300, 30));
        regitPanel.setBackground(new Color(255, 255, 255));
        JButton touristBtn = new JButton("游客登录");
        touristBtn.setBackground(new Color(0, 200, 0));
        touristBtn.setForeground(Color.white);
        JButton regitText = new JButton("没有账户？立即注册！");
        regitText.setBackground(Color.white);
        regitText.setBorder(null);
        regitText.setPreferredSize(new Dimension(200,30));
        regitText.setFont(new Font("黑体", Font.ITALIC, 12));
        regitText.addActionListener(e->new LoginListenEvents(flag));
        touristBtn.addActionListener(e->new LoginListenEvents(this,1));
        regitPanel.add(touristBtn);
        regitPanel.add(regitText);
        centerPanel.add(centerTop);
        centerPanel.add(centerBottom);
        centerPanel.add(loginPanel);
        centerPanel.add(regitPanel);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
}
