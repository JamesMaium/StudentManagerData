package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.UserViewEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 * 用户界面 首页
 */
public class UserView extends JFrame {
    final static private int TOP_PANEL_HEIGHT = 80;
    final static private int LEFT_PANEL_WIDTH = 200;
    final static private int BOTTOM_PANEL_WIDTH = 100;
    final static private String[] VEGETABLE = {"美食", "超市便利店", "水果", "下午茶", "甜点饮品", "买菜", "中午饭", "汉堡", "晚饭", "夜宵"};
    private final JPanel centerPanel = new JPanel();
    private final CardLayout cardLayout = new CardLayout();
    private final FlowLayout flow = new FlowLayout(FlowLayout.LEFT);
    private final UserView flag = this;
    public int id;
    public JLabel price = new JLabel("0");
    public JLabel shopNum = new JLabel("0");
    public HashMap<String,String> tempMapCars = new HashMap<>();
    public String userName;

    public UserView(String resUserName, int id) {
        this.id = id;
        this.userName = resUserName;
        FlowLayout flowLayout = new FlowLayout(FlowLayout.LEFT);
        flowLayout.setHgap(0);
        flowLayout.setVgap(0);
        setIconImage(new ImageIcon(JFrameConfig.LOGIN_WINDOW_ICON).getImage());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle(JFrameConfig.USER_UI_TITLE + resUserName);
        JPanel topJpanel = new JPanel();
        JLabel topJpanelImg = new JLabel();
        topJpanelImg.setIcon(new ImageIcon(JFrameConfig.USER_UI_TOP_IMG));
        topJpanel.setLayout(flowLayout);
        topJpanel.setPreferredSize(new Dimension(JFrameConfig.WIDTH, TOP_PANEL_HEIGHT));
        topJpanel.add(topJpanelImg);
        add(topJpanel, BorderLayout.NORTH);
        JPanel leftPanel = new JPanel();
        leftPanel.setPreferredSize(new Dimension(LEFT_PANEL_WIDTH, JFrameConfig.HEIGHT - TOP_PANEL_HEIGHT));
        for (String s : VEGETABLE) {
            JButton jButton = new JButton(s);
            jButton.setBorder(null);
            jButton.addActionListener(this::buttonListen);
            jButton.setFont(new Font("黑体", Font.PLAIN, 16));
            jButton.setBackground(new Color(255, 255, 255));
            leftPanel.add(jButton);
        }
        centerPanel.setLayout(cardLayout);
        centerPanel.add(VEGETABLE[0], shopItem(VEGETABLE[0]));
        centerPanel.add(VEGETABLE[1], shopItem(VEGETABLE[1]));
        centerPanel.add(VEGETABLE[2], shopItem(VEGETABLE[2]));
        centerPanel.add(VEGETABLE[3], shopItem(VEGETABLE[3]));
        centerPanel.add(VEGETABLE[4], shopItem(VEGETABLE[4]));
        centerPanel.add(VEGETABLE[5], shopItem(VEGETABLE[5]));
        centerPanel.add(VEGETABLE[6], shopItem(VEGETABLE[6]));
        centerPanel.add(VEGETABLE[7], shopItem(VEGETABLE[7]));
        centerPanel.add(VEGETABLE[8], shopItem(VEGETABLE[8]));
        centerPanel.add(VEGETABLE[9], shopItem(VEGETABLE[9]));
        FlowLayout bottomFlow = new FlowLayout(FlowLayout.LEFT);
        bottomFlow.setHgap(0);
        bottomFlow.setVgap(0);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setPreferredSize(new Dimension(JFrameConfig.WIDTH- LEFT_PANEL_WIDTH, BOTTOM_PANEL_WIDTH));
        bottomPanel.setBackground(new Color(255, 255, 255));
        bottomPanel.setLayout(bottomFlow);

        JButton userCenter = new JButton("个人中心");
        userCenter.setPreferredSize(new Dimension(200,100));
        userCenter.setBackground(new Color(5, 33, 208));
        userCenter.setForeground(new Color(255,255,255));
        userCenter.setFont(new Font("黑体",Font.BOLD,18));
        userCenter.setBorder(null);
        userCenter.addActionListener(e-> {
            try {
                new UserCenter(flag);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
        JPanel leftUser = new JPanel();
        leftUser.setPreferredSize(new Dimension( 829,100));
        leftUser.setBackground(new Color(255, 255, 255));
        leftUser.setLayout(bottomFlow);
        leftUser.add(userCenter);

        Font font = new Font("黑体", Font.BOLD, 20);
        JLabel priceText = new JLabel("总价:");
        JLabel shopNumText = new JLabel("数量:");
        priceText.setFont(font);
        shopNumText.setFont(font);
        price.setFont(font);
        shopNum.setFont(font);
        shopNumText.setPreferredSize(new Dimension(60, 100));
        shopNum.setPreferredSize(new Dimension(150, 100));
        priceText.setPreferredSize(new Dimension(60, 100));
        price.setPreferredSize(new Dimension(150, 100));
        JButton numBtn = new JButton("结算");
        numBtn.setFont(new Font("黑体", Font.BOLD, 16));
        numBtn.setBorder(null);
        numBtn.setBackground(new Color(255, 0, 0));
        numBtn.setForeground(new Color(255, 255, 255));
        numBtn.setPreferredSize(new Dimension(200, 100));
        numBtn.addActionListener(e -> new UserViewEvents(price, flag, id,tempMapCars));
        bottomPanel.add(leftUser);
        bottomPanel.add(priceText);
        bottomPanel.add(price);
        bottomPanel.add(shopNumText);
        bottomPanel.add(shopNum);
        bottomPanel.add(numBtn);
        GridLayout gridLayout = new GridLayout();
        gridLayout.setRows(10);
        leftPanel.setLayout(gridLayout);
        add(leftPanel, BorderLayout.WEST);
        centerPanel.setPreferredSize(new Dimension(JFrameConfig.WIDTH - LEFT_PANEL_WIDTH, JFrameConfig.HEIGHT - TOP_PANEL_HEIGHT));
        JScrollPane jScrollPane = new JScrollPane(centerPanel);
        jScrollPane.setOpaque(false);
        jScrollPane.getViewport().setOpaque(false);
        jScrollPane.setBorder(null);
        add(centerPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
        setSize(JFrameConfig.WIDTH, JFrameConfig.HEIGHT);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new LoginView();
            }
        });
    }

    /**
     * 按钮监听事件
     */
    public void buttonListen(ActionEvent event) {
        String btnText = event.getActionCommand();
        switch (btnText) {
            case "美食":
                cardLayout.show(centerPanel, "美食");
                repaint();
                break;
            case "超市便利店":
                cardLayout.show(centerPanel, "超市便利店");
                repaint();
                break;
            case "水果":
                cardLayout.show(centerPanel, "水果");
                repaint();
                break;
            case "下午茶":
                cardLayout.show(centerPanel, "下午茶");
                repaint();
                break;
            case "甜点饮品":
                cardLayout.show(centerPanel, "甜点饮品");
                repaint();
                break;
            case "买菜":
                cardLayout.show(centerPanel, "买菜");
                repaint();
                break;
            case "中午饭":
                cardLayout.show(centerPanel, "中午饭");
                repaint();
                break;
            case "汉堡":
                cardLayout.show(centerPanel, "汉堡");
                repaint();
                break;
            case "晚饭":
                cardLayout.show(centerPanel, "晚饭");
                repaint();
                break;
            case "夜宵":
                cardLayout.show(centerPanel, "夜宵");
                repaint();
                break;
            default:
                System.err.println("err");
                break;
        }
    }

    /**
     * 超市布局
     */
    public JScrollPane shopItem(String text) {
        flow.setHgap(0);
        flow.setVgap(0);
        FlowLayout cardPanelFlow = new FlowLayout();
        cardPanelFlow.setHgap(0);
        cardPanelFlow.setVgap(-1);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.getVerticalScrollBar().setUnitIncrement(20);
        scrollPane.setBorder(null);
        scrollPane.setOpaque(false);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        JPanel cardPanel = new JPanel();
        try {
            List<HashMap<String, String>> shop = new MysqlConnUtil().getShop(text);
            cardPanel.setPreferredSize(new Dimension(JFrameConfig.WIDTH- LEFT_PANEL_WIDTH, shop.size() * 200));
            for (HashMap<String, String> maps : shop) {
                JLabel shopItem = new JLabel();
                JLabel imgLable = new JLabel();
                JLabel titleLable = new JLabel();
                JLabel priceLable = new JLabel();
                JButton orderIndex = new JButton("加入购物车");
                orderIndex.setPreferredSize(new Dimension(100, 40));
                orderIndex.addActionListener(e -> new UserViewEvents(maps.get("id"), shopNum, price, flag,tempMapCars));
                orderIndex.setBackground(new Color(103, 194, 58));
                orderIndex.setBorder(null);
                orderIndex.setForeground(new Color(255, 255, 255));
                titleLable.setText(maps.get("title"));
                titleLable.setPreferredSize(new Dimension(400, 50));
                titleLable.setFont(new Font("黑体", Font.PLAIN, 20));
                priceLable.setText(maps.get("price"));
                priceLable.setPreferredSize(new Dimension(400, 50));
                priceLable.setFont(new Font("黑体", Font.PLAIN, 26));
                priceLable.setForeground(new Color(245, 108, 108));
                imgLable.setPreferredSize(new Dimension(200, 200));
                imgLable.setIcon(new ImageIcon(JFrameConfig.TEST_IMG));
                shopItem.setPreferredSize(new Dimension(JFrameConfig.WIDTH - LEFT_PANEL_WIDTH - 2, 200));
                shopItem.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255), 1));
                shopItem.setLayout(flow);
                shopItem.add(imgLable);
                shopItem.add(titleLable);
                shopItem.add(priceLable);
                shopItem.add(orderIndex);
                cardPanel.add(shopItem);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        cardPanel.setLayout(cardPanelFlow);
        scrollPane.setViewportView(cardPanel);
        return scrollPane;
    }
}
