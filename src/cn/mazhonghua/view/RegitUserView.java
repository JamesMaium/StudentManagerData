package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.dao.RegitUserDataDao;
import cn.mazhonghua.service.ReigtUserEvents;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * @author JamesMaium
 * 用户注册 Ui
 */
public class RegitUserView extends JFrame {
    public RegitUserView(JFrame jFrame) {
        JPanel topPanel = new JPanel();
        topPanel.setPreferredSize(new Dimension(400, 50));
        JLabel topPanelLanbleText = new JLabel(JFrameConfig.REGIT_FRAME_TITLE);
        Font font = new Font("黑体", Font.BOLD, 24);
        topPanelLanbleText.setFont(font);
        topPanel.add(topPanelLanbleText);

        JPanel centerPanel = new JPanel();
        centerPanel.setPreferredSize(new Dimension(400, 450));
        centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        JPanel userInputPanel1 = new JPanel();
        JPanel userInputPanel2 = new JPanel();
        JPanel userInputPanel3 = new JPanel();
        JPanel userInputPanel4 = new JPanel();

        userInputPanel1.setPreferredSize(new Dimension(300, 40));
        userInputPanel1.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel userNameLable = new JLabel("用户名:");
        Font inputFont = new Font("黑体", Font.BOLD, 16);
        userNameLable.setFont(inputFont);
        userNameLable.setPreferredSize(new Dimension(80, 30));
        JTextField userNameText = new JTextField();
        userNameText.setPreferredSize(new Dimension(200, 30));
        userInputPanel1.add(userNameLable);
        userInputPanel1.add(userNameText);

        userInputPanel2.setPreferredSize(new Dimension(300, 40));
        userInputPanel2.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel userPawdLable = new JLabel("密  码:");
        userPawdLable.setFont(inputFont);
        userPawdLable.setPreferredSize(new Dimension(80, 30));
        JPasswordField userPawdText = new JPasswordField();
        userPawdText.setPreferredSize(new Dimension(200, 30));

        userInputPanel2.add(userPawdLable);
        userInputPanel2.add(userPawdText);

        userInputPanel3.setPreferredSize(new Dimension(300, 40));
        userInputPanel3.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel userPawdAgainLable = new JLabel("确认密码:");
        userPawdAgainLable.setFont(inputFont);
        userPawdAgainLable.setPreferredSize(new Dimension(80, 30));
        JPasswordField userPawdAgainText = new JPasswordField();
        userPawdAgainText.setPreferredSize(new Dimension(200, 30));

        userInputPanel3.add(userPawdAgainLable);
        userInputPanel3.add(userPawdAgainText);

        userInputPanel4.setPreferredSize(new Dimension(300, 40));
        userInputPanel4.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel nameLable = new JLabel("姓名:");
        nameLable.setFont(inputFont);
        nameLable.setPreferredSize(new Dimension(80, 30));
        JTextField nameText = new JTextField();
        nameText.setPreferredSize(new Dimension(200, 30));
        userInputPanel4.add(nameLable);
        userInputPanel4.add(nameText);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        buttonsPanel.setPreferredSize(new Dimension(300, 100));
        JButton regitBtn = new JButton("注册");
        regitBtn.setPreferredSize(new Dimension(300, 30));

        JLabel reBackLable = new JLabel("已有账户，返回登录");
        reBackLable.setFont(new Font("黑体", Font.ITALIC, 14));

        regitBtn.addActionListener((e) -> {
            String password = new String(userPawdText.getPassword());
            String againPassword = new String(userPawdAgainText.getPassword());
            RegitUserDataDao userDataDao = new RegitUserDataDao();
            userDataDao.setUserName(userNameText.getText());
            userDataDao.setUserPassword(password);
            userDataDao.setUserAgainPassword(againPassword);
            userDataDao.setName(nameText.getText());

            if (userNameText.getText().toCharArray().length == 0) {
                JOptionPane.showMessageDialog(this, "请输入用户名", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            if (password.toCharArray().length == 0 || againPassword.toCharArray().length == 0) {
                JOptionPane.showMessageDialog(this, "请输入密码", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            if (!password.equals(againPassword)) {
                JOptionPane.showMessageDialog(this, "两次输入的密码不一样，请核对！", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
                return;
            }

            if (nameText.getText().toCharArray().length == 0) {
                JOptionPane.showMessageDialog(this, "请输入姓名", "JamesMa提示", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            new ReigtUserEvents(userDataDao, this,jFrame);
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                jFrame.setEnabled(true);
            }
        });
        reBackLable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                //  单机返回lable 后操作
                jFrame.setEnabled(true);
                dispose();
            }
        });

        buttonsPanel.add(regitBtn);
        buttonsPanel.add(reBackLable);
        centerPanel.add(userInputPanel1);
        centerPanel.add(userInputPanel2);
        centerPanel.add(userInputPanel3);
        centerPanel.add(userInputPanel4);
        centerPanel.add(buttonsPanel);
        add(topPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);
        setSize(400, 500);
        setTitle(JFrameConfig.REGIT_TITLE);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
}
