package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.AdminUserManagerEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 * 后台用户管理 UI
 */
public class AdminUserManager extends JFrame {
    private final static String TITLE = "用户管理中心 --登录管理员账户:";
    Font tableFont = new Font("黑体", Font.PLAIN, 16);
    public String[] head = {"id","用户名","姓名","用户权限","用户余额","注册时间"};
    public int pageSize = 10;
    public int currPage = 1;

    public String[][] getData(int currPage,int pageSize) throws SQLException {
        List<HashMap<String, String>> allUser = new MysqlConnUtil().getAllUser(currPage, pageSize);
        String[][] data = new String[allUser.size()][head.length];
        int a = 0;
        for(HashMap<String,String> map:allUser){
            data[a][0] = map.get("id");
            data[a][1] = map.get("userName");
            data[a][2] = map.get("userTrueName");
            data[a][3] = map.get("jurisdiction");
            data[a][4] = map.get("balance");
            data[a][5] = map.get("regitTime");
            a++;
        }
        return data;
    }

    public AdminUserManager(AdminIndex flag, String name) throws SQLException {

        flag.setEnabled(false);
        FlowLayout flow = new FlowLayout(FlowLayout.CENTER);
        flow.setVgap(0);
        flow.setHgap(0);

        JPanel jPanel = new JPanel();
        jPanel.setLayout(flow);
        jPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT));
        add(jPanel);

        String[][] data = getData(currPage, pageSize);


        DefaultTableModel dtm = new DefaultTableModel(data,head);
        JTable table = new JTable();
        table.setModel(dtm);
        table.getColumn("id").setMaxWidth(40);
        table.getColumn("id").setMinWidth(40);
        table.getColumn("注册时间").setMaxWidth(200);
        table.getColumn("注册时间").setMinWidth(200);
        table.setRowHeight(40);
        table.setFont(tableFont);

        JScrollPane jScrollPane = new JScrollPane(table);
        jScrollPane.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT - 100));
        jPanel.add(jScrollPane);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100,JFrameConfig.ADMIN_USER_MANAGER_HEIGHT));
        bottomPanel.setLayout(flow);
        jPanel.add(bottomPanel);



        JLabel numLable = new JLabel();
        numLable.setPreferredSize(new Dimension(150,30));
        numLable.setText("第"+currPage+"页,每页："+pageSize+"条");

        JButton before = new JButton("上一页");
        before.setPreferredSize(new Dimension(100,30));
        bottomPanel.add(before);
        before.addActionListener(e->new AdminUserManagerEvents(table,this,0,numLable));


        bottomPanel.add(numLable);

        JButton next = new JButton("下一页");
        next.setPreferredSize(new Dimension(100,30));
        bottomPanel.add(next);
        next.addActionListener(e->new AdminUserManagerEvents(table,this,1,numLable));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                flag.setEnabled(true);
            }
        });

        setLayout(flow);
        setBackground(new Color(255, 255, 255));
        setTitle(TITLE+name);
        setSize(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
}
