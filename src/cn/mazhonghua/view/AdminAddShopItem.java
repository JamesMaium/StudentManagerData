package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.dao.ShopItemDao;
import cn.mazhonghua.service.AdminAddShopItemEvents;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author JamesMaium
 * 添加商品UI
 */
public class AdminAddShopItem extends JFrame {

    FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
    private final static String TITLE = "添加商品页面：";
    Font miniFont = new Font("黑体", Font.PLAIN, 16);
    private final static int ITEM_PANEL_HEIGHT = 80;
    private final static int ITEM_PANEL_INPUT_HEIGHT = 40;
    private String[] ARR_TYPE = {"美食", "超市便利店", "水果", "下午茶", "甜点饮品", "买菜", "中午饭", "汉堡", "晚饭", "夜宵"};
    public JComboBox<String> type = new JComboBox<>();
    public JTextField priceField = new JTextField();
    public JTextField nameField = new JTextField();
    public AdminAddShopItem(AdminShopManager flag, String name) {
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);

        JPanel panel = new JPanel();
        panel.setBackground(Color.white);
        panel.setLayout(flowLayout);
        panel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT));
        add(panel);

        FlowLayout leftFlow = new FlowLayout(FlowLayout.LEFT);
        leftFlow.setVgap(0);
        leftFlow.setHgap(0);

        JPanel itemPanel = new JPanel();
        itemPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, ITEM_PANEL_HEIGHT));
        itemPanel.setBackground(Color.white);
        panel.add(itemPanel);
        JLabel nameLable = new JLabel("名称：");
        nameLable.setPreferredSize(new Dimension(80, 50));
        nameLable.setFont(miniFont);
        nameField.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 220, ITEM_PANEL_INPUT_HEIGHT));
        itemPanel.add(nameLable);
        itemPanel.add(nameField);

        JPanel itemPanel1 = new JPanel();
        itemPanel1.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, ITEM_PANEL_HEIGHT));
        itemPanel1.setBackground(Color.white);
        panel.add(itemPanel1);
        JLabel priceLable = new JLabel("价格：");
        priceLable.setPreferredSize(new Dimension(80, 50));
        priceLable.setFont(miniFont);
        priceField.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 220, ITEM_PANEL_INPUT_HEIGHT));
        itemPanel1.add(priceLable);
        itemPanel1.add(priceField);

        JPanel itemPanel2 = new JPanel();
        itemPanel2.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, ITEM_PANEL_HEIGHT));
        itemPanel2.setBackground(Color.white);
        panel.add(itemPanel2);
        JLabel typeLable = new JLabel("属性：");
        typeLable.setPreferredSize(new Dimension(80, 50));
        typeLable.setFont(miniFont);
        for (String s : ARR_TYPE) {
            type.addItem(s);
        }
        type.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 220, ITEM_PANEL_INPUT_HEIGHT));
        itemPanel2.add(typeLable);
        itemPanel2.add(type);


        JPanel itemPanel3 = new JPanel();
        itemPanel3.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, ITEM_PANEL_HEIGHT));
        itemPanel3.setBackground(Color.white);
        panel.add(itemPanel3);
        JButton addBtn = new JButton("添加");
        addBtn.setPreferredSize(new Dimension(130, 40));
        addBtn.setBorder(null);
        addBtn.setForeground(Color.WHITE);
        addBtn.setBackground(new Color(28, 168, 35));
        addBtn.addActionListener(e -> {
            ShopItemDao shopItem = new ShopItemDao();
            String dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            shopItem.setName(nameField.getText());
            shopItem.setPrice(priceField.getText());
            shopItem.setDateTime(dateTime);
            shopItem.setType(ARR_TYPE[type.getSelectedIndex()]);
            new AdminAddShopItemEvents(this, shopItem);
        });
        itemPanel3.add(addBtn);


        setLayout(flowLayout);
        setBackground(Color.white);
        setBackground(new Color(255, 255, 255));
        setTitle(TITLE + name);
        setSize(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
}
