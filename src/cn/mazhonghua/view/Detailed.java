package cn.mazhonghua.view;

import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author JamesMaium
 * 下单明细 UI
 */
public class Detailed extends JFrame {
    public Detailed(JFrame flag, HashMap<String, String> map) throws SQLException {
        flag.setEnabled(false);
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setHgap(0);
        flowLayout.setVgap(0);
        FlowLayout flowLayout1 = new FlowLayout(FlowLayout.CENTER);
        flowLayout1.setHgap(20);
        flowLayout1.setVgap(20);
        int size = map.size();
        String[] tableHead = {"id", "名称","价格","数量"};
        String[][] data = new String[size][tableHead.length];
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Map.Entry<String, String> next = iterator.next();
            String key = next.getKey();
            String value = next.getValue();
            HashMap<String, String> map1 = new MysqlConnUtil().queryNameAndPrice(new Integer(key));
            data[i][0] = key;
            data[i][1] = map1.get("title");
            data[i][2] = map1.get("price");
            data[i][3] = value;
            i++;
        }
        DefaultTableModel tableModel = new DefaultTableModel(data, tableHead);
        JTable table = new JTable();
        table.setPreferredSize(new Dimension(400, size * 20));
        table.setModel(tableModel);
        table.setRowHeight(20);
        table.getColumn("id").setMinWidth(20);
        table.getColumn("id").setMaxWidth(20);
        table.getColumn("价格").setMinWidth(80);
        table.getColumn("价格").setMaxWidth(80);
        table.getColumn("数量").setMinWidth(30);
        table.getColumn("数量").setMaxWidth(30);
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setViewportView(table);
        JPanel jPanel = new JPanel();
        jPanel.setSize(500, 500);
        jPanel.setLayout(flowLayout1);
        add(jPanel);
        jPanel.add(jScrollPane);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                flag.setEnabled(true);
            }
        });
        setSize(500, 500);
        setTitle("下单明细");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(flowLayout);
        setResizable(false);
        setVisible(true);
    }
}
