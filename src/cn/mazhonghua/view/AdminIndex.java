package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.AdminIndexEvents;
import cn.mazhonghua.service.ShopManagerEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

/**
 * @author JamesMaium
 * 后台管理员 UI
 */
public class AdminIndex extends JFrame {

    private final static int HEIGHT = 50;
    private final static int WIDTH = 100;
    private final static int BTN_PANEL_HEIGHT = 50;

    // 累计会员
    public JLabel userCount = new JLabel("...");
    // 今日流水
    public JLabel todayMoney = new JLabel("...");
    // 管理员数量
    public JLabel adminCount = new JLabel("...");
    // 商品总数
    public JLabel shopCount = new JLabel("...");

    public AdminIndex(String name) {
        FlowLayout flow = new FlowLayout(FlowLayout.CENTER);
        flow.setVgap(0);
        flow.setHgap(0);
        FlowLayout leftFlow = new FlowLayout(FlowLayout.LEFT);
        leftFlow.setVgap(0);
        leftFlow.setHgap(0);

        JPanel panel = new JPanel();
        panel.setLayout(flow);
        panel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH, JFrameConfig.ADMIN_HEIGHT));
        panel.setBackground(new Color(255, 255, 255));
        add(panel);


        JLabel title = new JLabel(JFrameConfig.TITLE, JLabel.CENTER);
        Font font = new Font("黑体", Font.BOLD, 20);
        Font itemFont = new Font("黑体", Font.PLAIN, 16);
        Color color = new Color(0, 255, 111);
        Color rightLableColor = new Color(70, 70, 70);
        Color btn = new Color(81, 106, 229);
        Color btn2 = new Color(192, 81, 229);
        Color btnFontColor = new Color(255, 255, 255);
        title.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, 80));
        title.setFont(font);
        title.setForeground(color);
        panel.add(title);

        JPanel itemPanel = new JPanel();
        itemPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, HEIGHT));
        itemPanel.setBackground(new Color(255, 255, 255));
        itemPanel.setLayout(leftFlow);
        panel.add(itemPanel);
        JLabel userCountLable = new JLabel("累计会员:");
        userCountLable.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        userCountLable.setFont(itemFont);

        userCount.setFont(itemFont);
        userCount.setForeground(rightLableColor);
        userCount.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 200, HEIGHT));
        itemPanel.add(userCountLable);
        itemPanel.add(userCount);


        JPanel itemPanel2 = new JPanel();
        itemPanel2.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, HEIGHT));
        itemPanel2.setBackground(new Color(255, 255, 255));
        itemPanel2.setLayout(leftFlow);
        panel.add(itemPanel2);
        JLabel todayMoneyLable = new JLabel("今日流水:");
        todayMoneyLable.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        todayMoneyLable.setFont(itemFont);

        todayMoney.setFont(itemFont);
        todayMoney.setForeground(rightLableColor);
        todayMoney.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 200, HEIGHT));
        itemPanel2.add(todayMoneyLable);
        itemPanel2.add(todayMoney);


        JPanel itemPanel3 = new JPanel();
        itemPanel3.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, HEIGHT));
        itemPanel3.setBackground(new Color(255, 255, 255));
        itemPanel3.setLayout(leftFlow);
        panel.add(itemPanel3);
        JLabel adminCountLable = new JLabel("管理员数量:");
        adminCountLable.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        adminCountLable.setFont(itemFont);

        adminCount.setFont(itemFont);
        adminCount.setForeground(rightLableColor);
        adminCount.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 200, HEIGHT));
        itemPanel3.add(adminCountLable);
        itemPanel3.add(adminCount);


        JPanel itemPanel4 = new JPanel();
        itemPanel4.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, HEIGHT));
        itemPanel4.setBackground(new Color(255, 255, 255));
        itemPanel4.setLayout(leftFlow);
        panel.add(itemPanel4);
        JLabel shopCountLable = new JLabel("商品总数:");
        shopCountLable.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        shopCountLable.setFont(itemFont);

        shopCount.setFont(itemFont);
        shopCount.setForeground(rightLableColor);
        shopCount.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 200, HEIGHT));
        itemPanel4.add(shopCountLable);
        itemPanel4.add(shopCount);

        new AdminIndexEvents(this);


        JPanel itemPanel5 = new JPanel();
        itemPanel5.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, BTN_PANEL_HEIGHT));
        itemPanel5.setBackground(new Color(255, 255, 255));
        itemPanel5.setLayout(leftFlow);
        panel.add(itemPanel5);
        JButton shopAllUsers = new JButton("显示所有用户");
        shopAllUsers.setBackground(btn);
        shopAllUsers.setForeground(btnFontColor);
        shopAllUsers.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
        shopAllUsers.setPreferredSize(new Dimension(100, 30));
        shopAllUsers.addActionListener(e -> new AdminIndexEvents(this, name));
        itemPanel5.add(shopAllUsers);

//        JButton fixUserInfo = new JButton("更新用户");
//        fixUserInfo.setBackground(btn);
//        fixUserInfo.setForeground(btnFontColor);
//        fixUserInfo.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
//        fixUserInfo.setPreferredSize(new Dimension(100, 30));
//        itemPanel5.add(fixUserInfo);

        JPanel itemPanel6 = new JPanel();
        itemPanel6.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, BTN_PANEL_HEIGHT));
        itemPanel6.setBackground(new Color(255, 255, 255));
        itemPanel6.setLayout(leftFlow);
        panel.add(itemPanel6);
        JButton shopManager = new JButton("商品管理");
        shopManager.setBackground(btn);
        shopManager.setForeground(btnFontColor);
        shopManager.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
        shopManager.setPreferredSize(new Dimension(100, 30));
        shopManager.addActionListener(e -> new ShopManagerEvents(this, name));
        itemPanel6.add(shopManager);


        JPanel itemPanel7 = new JPanel();
        itemPanel7.setPreferredSize(new Dimension(JFrameConfig.ADMIN_WIDTH - 100, BTN_PANEL_HEIGHT));
        itemPanel7.setBackground(new Color(255, 255, 255));
        itemPanel7.setLayout(leftFlow);
        panel.add(itemPanel7);
        JButton datilManager = new JButton("历史订单");
        JButton datilToday = new JButton("今日订单");
        datilManager.setBackground(btn);
        datilManager.setForeground(btnFontColor);
        datilManager.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
        datilManager.setPreferredSize(new Dimension(100, 30));
        datilToday.setBackground(btn2);
        datilToday.setForeground(btnFontColor);
        datilToday.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
        datilToday.setPreferredSize(new Dimension(100, 30));
        itemPanel7.add(datilManager);
        itemPanel7.add(datilToday);
        datilManager.addActionListener(e->new AdminBill(name));
        datilToday.addActionListener(e->new AdminTodayDatil(name));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new LoginView();
            }
        });

        setLayout(flow);
        setBackground(new Color(255, 255, 255));
        setTitle(JFrameConfig.ADMIN_INDEX_TITLE + name);
        setSize(JFrameConfig.ADMIN_WIDTH, JFrameConfig.ADMIN_HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
}
