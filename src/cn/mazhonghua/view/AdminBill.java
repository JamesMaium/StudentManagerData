package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.AdminBillEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 */
public class AdminBill extends JFrame {
    public int currPage = 1;
    public int pageSize = 10;
    public JTable table = new JTable();
    public String[] head = {"订单ID", "商品名称", "商品单价", "商品类型", "姓名", "用户名", "下单数量", "下单时间"};

    public AdminBill(String name) {
        FlowLayout flow = new FlowLayout(FlowLayout.CENTER);
        flow.setVgap(0);
        flow.setHgap(0);

        JPanel panel = new JPanel();
        panel.setLayout(flow);
        panel.setBackground(new Color(255, 255, 255));
        panel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT));
        add(panel);

        JPanel topPanel = new JPanel();
        topPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT - 100));
        topPanel.setBackground(new Color(255, 255, 255));
        panel.add(topPanel);

        new AdminBillEvents(this);
        table.setRowHeight(30);
        JScrollPane jScrollPane = new JScrollPane(table);
        jScrollPane.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT - 120));
        topPanel.add(jScrollPane);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(flow);
        bottomPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, 100));
        bottomPanel.setBackground(new Color(255, 255, 255));
        panel.add(bottomPanel);

        JButton before = new JButton("上一页");
        JLabel text = new JLabel("第" + currPage + "页，每页" + pageSize + "条", JLabel.CENTER);
        JButton next = new JButton("下一页");
        before.addActionListener(e -> {
            try {
                new AdminBillEvents(this, 0);
                text.setText("第" + currPage + "页，每页" + pageSize + "条");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
        next.addActionListener(e -> {
            try {
                new AdminBillEvents(this, 1);
                text.setText("第" + currPage + "页，每页" + pageSize + "条");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });

        before.setPreferredSize(new Dimension(100, 40));
        text.setPreferredSize(new Dimension(200, 40));
        next.setPreferredSize(new Dimension(100, 40));
        bottomPanel.add(before);
        bottomPanel.add(text);
        bottomPanel.add(next);

        setLayout(flow);
        setBackground(new Color(255, 255, 255));
        setTitle(JFrameConfig.ADMIN_INDEX_TITLE + name);
        setSize(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }
}
