package cn.mazhonghua.view;

import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * @author JamesMaium
 * 结算 UI
 */
public class Settlement extends JFrame {
    final static private String TEMP = "0";
    public Settlement(JLabel priceLable, UserView flag , int id, HashMap<String,String> map){
        flag.setEnabled(false);
        String price = priceLable.getText();
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);
        Font font = new Font("黑体",Font.BOLD,20);
        Font tipsFont = new Font("黑体",Font.BOLD,16);
        Font btnFont = new Font("黑体",Font.BOLD,18);
        JLabel title = new JLabel("富掌柜收银台", SwingConstants.CENTER);
        title.setFont(font);
        title.setPreferredSize(new Dimension(300,100));
        add(title);

        JButton lookMx = new JButton("查看明细");
        lookMx.setPreferredSize(new Dimension(100,30));
        lookMx.setBackground(new Color(96, 239, 26));
        lookMx.setForeground(new Color(255,255,255));
        lookMx.setBorder(null);
        lookMx.addActionListener(e->{
            try {
                new Detailed(this,map);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
        add(lookMx);

        JLabel tips = new JLabel("当前结算金额", SwingConstants.CENTER);
        tips.setFont(tipsFont);
        tips.setPreferredSize(new Dimension(300,100));
        add(tips);

        JLabel priceText = new JLabel(price, SwingConstants.CENTER);
        priceText.setForeground(new Color(245, 108, 108));
        priceText.setPreferredSize(new Dimension(300,100));
        priceText.setFont(font);
        add(priceText);

        JButton settlementBtn = new JButton("付款");
        settlementBtn.setPreferredSize(new Dimension(200,60));
        settlementBtn.setFont(btnFont);
        settlementBtn.setBorder(null);
        settlementBtn.setBackground(new Color(96, 239, 26));
        settlementBtn.addActionListener(e -> {
            try {
                settlementBtn.setBackground(new Color(103, 194, 58));
                String countText = flag.shopNum.getText();
                if(TEMP.equals(countText)){
                    JOptionPane.showMessageDialog(flag, "结算成功!您付款 0 元!", "0元付款", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                double userBalance = new MysqlConnUtil().queryUserBalance(id);
                double shopPrice = Double.parseDouble(price);
                if(userBalance<shopPrice){
                    JOptionPane.showMessageDialog(flag, "尴尬!账面余额不足", "付款失败", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                boolean checkOut = new MysqlConnUtil().userCheckOut(id, shopPrice);
                if(!checkOut){
                    JOptionPane.showMessageDialog(flag, "付款失败！", "付款失败", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(flag, "付款成功！", "付款成功", JOptionPane.WARNING_MESSAGE);
                new MysqlConnUtil().updateDatilStatus(flag.tempMapCars,id);
                flag.price.setText("0");
                flag.shopNum.setText("0");
                flag.tempMapCars.clear();
                flag.setEnabled(true);
                this.dispose();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
        add(settlementBtn);

        setTitle("收银台");
        setSize(300,500);
        setLayout(flowLayout);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                flag.setEnabled(true);
            }
        });
    }

}
