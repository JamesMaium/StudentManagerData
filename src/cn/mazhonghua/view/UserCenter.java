package cn.mazhonghua.view;

import cn.mazhonghua.service.UserCenterEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * @author JamesMaium
 * 个人中心 UI
 */
public class UserCenter extends JFrame {

    public JLabel balance = new JLabel();
    public JTextField recharge = new JTextField();
    public JPasswordField pawd = new JPasswordField();
    final static private int WDITH = 600;
    final static private int HEIGHT = 600;
    final static private int ITEM_HEIGHT = 60;
    final static private int ITEM_LABLES_HEIGHT = 60;
    public HashMap<String, String> userAllInfo;

    public UserCenter(UserView flag) throws SQLException {
        flag.setEnabled(false);
        this.userAllInfo = new MysqlConnUtil().getUserAllInfo(flag.id);

        FlowLayout flowLayout = new FlowLayout();
        flowLayout.setVgap(0);
        flowLayout.setHgap(10);
        FlowLayout itemFlow = new FlowLayout(FlowLayout.LEFT);
        itemFlow.setVgap(0);
        itemFlow.setHgap(0);

        JPanel jPanel = new JPanel();
        jPanel.setBackground(new Color(255, 255, 255));
        Font font = new Font("黑体", Font.PLAIN, 18);

        JPanel itemPanel1 = new JPanel();
        itemPanel1.setBackground(new Color(255, 255, 255));
        itemPanel1.setPreferredSize(new Dimension(WDITH - 100, ITEM_HEIGHT));
        itemPanel1.setLayout(itemFlow);
        jPanel.add(itemPanel1);
        JLabel nameLable = new JLabel("会员姓名:");
        nameLable.setPreferredSize(new Dimension(100, ITEM_LABLES_HEIGHT));
        nameLable.setFont(font);
        JLabel name = new JLabel(userAllInfo.get("userTrueName"));
        name.setPreferredSize(new Dimension(300, ITEM_LABLES_HEIGHT));
        name.setFont(font);
        itemPanel1.add(nameLable);
        itemPanel1.add(name);

        JPanel itemPanel2 = new JPanel();
        itemPanel2.setBackground(new Color(255, 255, 255));
        itemPanel2.setPreferredSize(new Dimension(WDITH - 100, ITEM_HEIGHT));
        itemPanel2.setLayout(itemFlow);
        jPanel.add(itemPanel2);
        JLabel regitLable = new JLabel("注册时间:");
        regitLable.setPreferredSize(new Dimension(100, ITEM_LABLES_HEIGHT));
        regitLable.setFont(font);
        JLabel regiter = new JLabel(userAllInfo.get("regitTime"));
        regiter.setPreferredSize(new Dimension(300, ITEM_LABLES_HEIGHT));
        regiter.setFont(font);
        itemPanel2.add(regitLable);
        itemPanel2.add(regiter);

        JPanel itemPanel3 = new JPanel();
        itemPanel3.setBackground(new Color(255, 255, 255));
        itemPanel3.setPreferredSize(new Dimension(WDITH - 100, ITEM_HEIGHT));
        itemPanel3.setLayout(itemFlow);
        jPanel.add(itemPanel3);
        JLabel accountLable = new JLabel("会员账号:");
        accountLable.setPreferredSize(new Dimension(100, ITEM_LABLES_HEIGHT));
        accountLable.setFont(font);
        JLabel account = new JLabel(userAllInfo.get("userName"));
        account.setPreferredSize(new Dimension(300, ITEM_LABLES_HEIGHT));
        account.setFont(font);
        itemPanel3.add(accountLable);
        itemPanel3.add(account);

        JPanel itemPanel4 = new JPanel();
        itemPanel4.setBackground(new Color(255, 255, 255));
        itemPanel4.setPreferredSize(new Dimension(WDITH - 100, ITEM_HEIGHT));
        itemPanel4.setLayout(itemFlow);
        jPanel.add(itemPanel4);
        JLabel userBalance = new JLabel("会员余额:");
        userBalance.setPreferredSize(new Dimension(100, ITEM_LABLES_HEIGHT));
        userBalance.setFont(font);
        balance.setText(userAllInfo.get("balance"));
        balance.setPreferredSize(new Dimension(300, ITEM_LABLES_HEIGHT));
        balance.setFont(font);
        itemPanel4.add(userBalance);
        itemPanel4.add(balance);

        JPanel itemPanel5 = new JPanel();
        itemPanel5.setBackground(new Color(255, 255, 255));
        itemPanel5.setPreferredSize(new Dimension(WDITH - 100, ITEM_HEIGHT));
        itemPanel5.setLayout(itemFlow);
        jPanel.add(itemPanel5);
        JLabel identityLable = new JLabel("当前身份:");
        identityLable.setPreferredSize(new Dimension(100, ITEM_LABLES_HEIGHT));
        identityLable.setFont(font);
        JLabel identity = new JLabel(userAllInfo.get("jurisdiction"));
        identity.setPreferredSize(new Dimension(300, ITEM_LABLES_HEIGHT));
        identity.setFont(font);
        itemPanel5.add(identityLable);
        itemPanel5.add(identity);

        JPanel itemPanel6 = new JPanel();
        itemPanel6.setBackground(new Color(255, 255, 255));
        itemPanel6.setPreferredSize(new Dimension(WDITH - 100, 40));
        itemPanel6.setLayout(itemFlow);
        jPanel.add(itemPanel6);

        JLabel rechargeLable = new JLabel("余额充值:");
        rechargeLable.setPreferredSize(new Dimension(100, 40));
        rechargeLable.setFont(font);
        rechargeLable.setForeground(new Color(255, 0, 0));
        recharge.setPreferredSize(new Dimension(200, 35));

        JButton done = new JButton("充值");
        done.setBorder(null);
        done.setBackground(new Color(58, 168, 1));
        done.setForeground(new Color(255, 255, 255));
        done.setPreferredSize(new Dimension(100, 35));
        done.addActionListener(e -> new UserCenterEvents(userAllInfo.get("id"), this, recharge.getText()));
        itemPanel6.add(rechargeLable);
        itemPanel6.add(recharge);
        itemPanel6.add(done);

        JPanel itemPanel7 = new JPanel();
        itemPanel7.setBackground(new Color(255, 255, 255));
        itemPanel7.setPreferredSize(new Dimension(WDITH - 100, 40));
        itemPanel7.setLayout(itemFlow);
        jPanel.add(itemPanel7);
        JLabel pawdLabel = new JLabel("修改密码:");
        pawdLabel.setPreferredSize(new Dimension(100, 40));
        pawdLabel.setFont(font);
        pawd.setPreferredSize(new Dimension(200, 35));
        JButton updatePawd = new JButton("修改");
        updatePawd.setBorder(null);
        updatePawd.setBackground(new Color(58, 168, 1));
        updatePawd.setForeground(new Color(255, 255, 255));
        updatePawd.setPreferredSize(new Dimension(100, 35));
        updatePawd.addActionListener(e -> new UserCenterEvents(this, new String(pawd.getPassword())));
        itemPanel7.add(pawdLabel);
        itemPanel7.add(pawd);
        itemPanel7.add(updatePawd);

        JPanel itemPanel8 = new JPanel();
        itemPanel8.setBackground(new Color(255, 255, 255));
        itemPanel8.setPreferredSize(new Dimension(WDITH - 100, ITEM_HEIGHT));
        itemPanel8.setLayout(itemFlow);
        jPanel.add(itemPanel8);
        JLabel mineLog = new JLabel("订单记录:");
        mineLog.setPreferredSize(new Dimension(100, ITEM_HEIGHT));
        mineLog.setFont(font);
        JButton mineLogBtn = new JButton("点击查看");
        mineLogBtn.setBorder(null);
        mineLogBtn.setBackground(new Color(26, 89, 208));
        mineLogBtn.setForeground(new Color(255, 255, 255));
        mineLogBtn.setPreferredSize(new Dimension(100, 35));
        mineLogBtn.addActionListener(e -> new UserCenterEvents(this));
        itemPanel8.add(mineLog);
        itemPanel8.add(mineLogBtn);

        add(jPanel);
        setTitle(flag.userName);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(flowLayout);
        setSize(WDITH, HEIGHT);
        jPanel.setPreferredSize(new Dimension(WDITH, HEIGHT));
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                flag.setEnabled(true);
            }
        });
    }
}
