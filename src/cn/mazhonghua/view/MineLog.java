package cn.mazhonghua.view;

import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 * 个人购买记录 UI
 */
public class MineLog extends JFrame {
    final static private int WIDTH = 900;
    final static private int HEIGHT = 600;

    public MineLog(UserCenter center) throws SQLException {
        center.setEnabled(false);
        int userId = new Integer(center.userAllInfo.get("id"));
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setHgap(0);
        flowLayout.setVgap(0);

        JPanel jPanel = new JPanel();
        jPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        jPanel.setLayout(flowLayout);
        jPanel.setBackground(new Color(255, 255, 255));

        List<HashMap<String, String>> userLogs = new MysqlConnUtil().getUsetLogs(userId);
        String[] head = {"标题", "价格", "数量", "下单时间"};
        String[][] data = new String[userLogs.size()][head.length];
        int a = 0;
        for (HashMap<String, String> userLog : userLogs) {
            data[a][0] = userLog.get("title");
            data[a][1] = userLog.get("price");
            data[a][2] = userLog.get("count");
            data[a][3] = userLog.get("dateTime");
            a++;
        }
        JTable table = new JTable(data, head);
        DefaultTableModel defaultTableModel = new DefaultTableModel(data, head);
        table.setModel(defaultTableModel);
        table.setRowHeight(30);
        Font font = new Font("黑体", Font.PLAIN, 16);
        table.setFont(font);
        table.getColumn("价格").setMaxWidth(100);
        table.getColumn("数量").setMaxWidth(50);
        table.getColumn("下单时间").setMaxWidth(180);
        table.getColumn("价格").setMinWidth(100);
        table.getColumn("数量").setMinWidth(50);
        table.getColumn("下单时间").setMinWidth(180);
        JScrollPane jScrollPane = new JScrollPane(table);
        jScrollPane.setPreferredSize(new Dimension(WIDTH - 100, HEIGHT));
        jPanel.add(jScrollPane);
        add(jPanel);
        setTitle("账单明细");
        setSize(WIDTH, HEIGHT);
        setLayout(flowLayout);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                center.setEnabled(true);
            }
        });
    }

}
