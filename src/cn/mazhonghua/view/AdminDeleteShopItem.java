package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.AdminDeleteShopItemEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author JamesMaium
 * 删除商品 UI
 */
public class AdminDeleteShopItem extends JFrame {
    private final static String TITLE = "删除商品：";
    public java.util.List<String> itemArr = new ArrayList<>();
    public List<String> IdArr = new ArrayList<>();
    Font font = new Font("黑体", Font.PLAIN, 16);
    public JComboBox<String> comboBox = new JComboBox<>();
    public JLabel title = new JLabel("剩余总数:", JLabel.CENTER);

    public AdminDeleteShopItem(String name) {
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);

        new AdminDeleteShopItemEvents(this);


        JPanel panel = new JPanel();
        panel.setLayout(flowLayout);
        panel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT));
        add(panel);

        JPanel topPanel = new JPanel();
        topPanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, 100));
        topPanel.setLayout(flowLayout);
        title.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, 100));
        title.setFont(font);
        topPanel.add(title);
        panel.add(topPanel);

        JPanel item1 = new JPanel();
        item1.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, 300));
        item1.setLayout(flowLayout);

        item1.add(comboBox);
        panel.add(item1);


        JPanel item2 = new JPanel();
        item2.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100, 100));
        item2.setLayout(flowLayout);

        JButton deleteBtn = new JButton("删除选中商品");
        deleteBtn.setPreferredSize(new Dimension(150, 40));
        deleteBtn.setBorder(null);
        deleteBtn.setBackground(Color.GREEN);
        deleteBtn.setForeground(Color.WHITE);
        deleteBtn.addActionListener(e ->new AdminDeleteShopItemEvents(this, 0));
        item2.add(deleteBtn);
        panel.add(item2);


        setLayout(flowLayout);
        setBackground(new Color(255, 255, 255));
        setTitle(TITLE + name);
        setSize(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

    }

}
