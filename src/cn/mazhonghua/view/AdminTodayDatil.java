package cn.mazhonghua.view;

import cn.mazhonghua.config.JFrameConfig;
import cn.mazhonghua.service.AdminTodayDatilEvents;
import cn.mazhonghua.util.MysqlConnUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author JamesMaium
 * 今日订单UI
 */
public class AdminTodayDatil extends JFrame {
    private final static String TITLE = "今日订单：";
    public String[] head = {"订单ID", "商品名称", "商品单价", "商品类型", "姓名", "用户名", "下单数量", "下单时间"};
    public int currPage = 1;
    public int pageSize = 20;


    public  AdminTodayDatil(String name){
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setVgap(0);
        flowLayout.setHgap(0);

        Font font = new Font("黑体", Font.PLAIN, 16);
        Font tableFont = new Font("黑体", Font.PLAIN, 14);

        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT));


        JPanel itemPenel = new JPanel();
        itemPenel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100,50));
        itemPenel.setLayout(flowLayout);
        panel.add(itemPenel);

        int datilUserCount = 0;
        try {
            datilUserCount = AdminTodayDatilEvents.getNum();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        JLabel title = new JLabel("今日下单人数："+datilUserCount,JLabel.LEFT);
        title.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100,50));
        title.setFont(font);
        itemPenel.add(title);

        JPanel tablePanel = new JPanel();
        tablePanel.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100,350));
        tablePanel.setLayout(flowLayout);
        panel.add(tablePanel);
        String[][] data = null;
        try {
            data = getData(currPage, pageSize, head.length);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DefaultTableModel dtm = new DefaultTableModel(data,head);
        JTable table = new JTable();
        table.setModel(dtm);
        table.setRowHeight(30);
        table.setFont(tableFont);
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(JFrameConfig.ADMIN_USER_MANAGER_WIDTH - 100,350));
        tablePanel.add(scrollPane);



        add(panel);

        setLayout(flowLayout);
        setBackground(new Color(255, 255, 255));
        setTitle(TITLE + name);
        setSize(JFrameConfig.ADMIN_USER_MANAGER_WIDTH, JFrameConfig.ADMIN_USER_MANAGER_HEIGHT);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }


    public String[][] getData(int currPage, int pageSize, int headLength) throws SQLException {
        List<HashMap<String, String>> datilData = new MysqlConnUtil().getTodayDatilUser(currPage, pageSize);
        String[][] data = new String[datilData.size()][headLength];
        int a = 0;
        for (HashMap<String, String> map : datilData) {
            data[a][0] = map.get("datilId");
            data[a][1] = map.get("shopTitle");
            data[a][2] = map.get("shopPrice");
            data[a][3] = map.get("shopType");
            data[a][4] = map.get("userTrueName");
            data[a][5] = map.get("userName");
            data[a][6] = map.get("count");
            data[a][7] = map.get("datilTime");
            a++;
        }
        return data;
    }
}
